import { Injectable } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from "angularfire2/firestore";
import { Subscription } from "rxjs/Subscription";
import { UsuarioProvider } from "../usuario/usuario";

@Injectable()
export class UbicacionProvider {
  taxista: AngularFirestoreDocument<any>;
  private watch: Subscription;

  constructor(
    private geoLocation: Geolocation,
    private _usuarioProv: UsuarioProvider,
    private afDB: AngularFirestore
  ) {}

  iniciarLizarTaxista() {
    this.taxista = this.afDB.doc(`/usuarios/${this._usuarioProv.clave}`);
  }

  iniciarGeoLocalizacion() {
    this.geoLocation
      .getCurrentPosition()
      .then((resp) => {
        // resp.coords.latitude
        // resp.coords.longitude
        this.updateCoordsDataBase(resp.coords);

        this.watch = this.geoLocation.watchPosition().subscribe((data) => {
          this.updateCoordsDataBase(data.coords);
        });
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  private updateCoordsDataBase(coords: any) {
    this.taxista.update({
      lat: coords.latitude,
      lng: coords.longitude,
      clave: this._usuarioProv.clave,
    });
  }

  detenerUbicacion() {
    try {
      this.watch.unsubscribe();
    } catch (e) {
      console.log(JSON.stringify(e));
    }
  }
}
