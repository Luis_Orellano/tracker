import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { UbicacionProvider } from "../../providers/ubicacion/ubicacion";
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { LoginPage } from "../login/login";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage {
  lat: number;
  lng: number;

  user: any = {};

  constructor(
    public navCtrl: NavController,
    private _ubicacionProv: UbicacionProvider,
    private _usuarioProv: UsuarioProvider
  ) {
    this._ubicacionProv.iniciarGeoLocalizacion();
    this._ubicacionProv.iniciarLizarTaxista();

    this._ubicacionProv.taxista.valueChanges().subscribe((data) => {
      this.user = data;
    });
  }

  salir() {
    this._ubicacionProv.detenerUbicacion();
    this._usuarioProv.borrarUsuario();
    this.navCtrl.setRoot(LoginPage);
  }
}
