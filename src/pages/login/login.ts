import { Component } from "@angular/core";
import { IonicPage, LoadingController, NavController } from "ionic-angular";
import { ViewChild } from "@angular/core";
import { Slides } from "ionic-angular";
import { AlertController } from "ionic-angular";
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { HomePage } from "../home/home";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html",
})
export class LoginPage {
  @ViewChild(Slides) slides: Slides;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private _usuarioProv: UsuarioProvider
  ) {}

  ionViewDidLoad() {
    this.slides.paginationType = "progress";
    this.slides.lockSwipes(true);
    this.slides.freeMode = false;
  }

  mostrarInput() {
    let alert = this.alertCtrl.create({
      title: "Ingrese el usuario",
      inputs: [
        {
          name: "username",
          placeholder: "Username",
        },
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
        },
        {
          text: "Ingresar",
          handler: (data) => {
            console.log(data);
            this.verificarUsuario(data.username);
          },
        },
      ],
    });
    alert.present();
  }

  verificarUsuario(clave: string) {
    let loading = this.loadingCtrl.create({
      content: "Verificando",
    });
    loading.present();

    this._usuarioProv.verificaUsuario(clave).then((existe) => {
      loading.dismiss();
      if (existe) {
        this.nextSlide();
      } else {
        this.alertCtrl
          .create({
            title: "Usuario incorrecto",
            subTitle: "Hable con el administrador o pruebe de nuevo",
            buttons: ["Aceptar"],
          })
          .present();
      }
    });
  }

  private nextSlide() {
    this.slides.lockSwipes(false);
    this.slides.freeMode = true;
    this.slides.slideNext();
    this.slides.lockSwipes(true);
    this.slides.freeMode = false;
  }

  ingresar() {
    this.navCtrl.setRoot(HomePage);
  }
}
